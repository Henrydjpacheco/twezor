from rest_framework import serializers  
from appTwezor.models import Foro_pg

class Foro_pgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Foro_pg
        fields = (
            'foro_id',
            'title',
            'content',
            'start_at',
            'finish_at',
            'author',
            'receptor',
        )