from rest_framework import serializers

from appTwezor.models import Comment
""""serializar mi objeto.  Convertir de Objetos(Diccionario o listas) a JSON"""
class Comment_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = (
            'id',
            'author',
            'content',
            'created_at'
        )