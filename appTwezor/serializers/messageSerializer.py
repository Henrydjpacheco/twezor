from rest_framework import serializers

from appTwezor.models import Message
""""serializar mi objeto.  Convertir de Objetos(Diccionario o listas) a JSON"""
class Message_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = (
            'content',
            'user',
            'conversation',
            'created_at',
            'is_readed'
        )