from rest_framework import serializers

from appTwezor.models import Chat_pg

class Chat_pgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat_pg
        fields = (
            'id_chat',
            'sender',
            'receptor',
            'created_at',

        )