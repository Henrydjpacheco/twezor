from rest_framework import serializers

from appTwezor.models import Team_pg

class Team_pgSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team_pg
        fields = (
            'team_id',
            'user_id',
            'title',
            'isActive'
        )