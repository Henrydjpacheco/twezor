from rest_framework import serializers

from appTwezor.models import Notification
""""serializar mi objeto.  Convertir de Objetos(Diccionario o listas) a JSON"""
class Notification_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = (
            'id',
            'receptor',
            'sender',
            'is_readed',
            'created_at'  
        )