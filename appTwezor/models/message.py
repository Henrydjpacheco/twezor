from django.db import models
from .user import User
from .chat_pg import Chat_pg

class Message(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.CharField(max_length=500)
    user = models.ForeignKey(User,related_name='message', on_delete=models.CASCADE)
    conversation = models.ForeignKey(Chat_pg,related_name='message', on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    is_readed = models.BooleanField(default=False)