from django.db import models
from .user import User

class Chat_pg(models.Model):
    id_chat = models.AutoField(primary_key=True)
    sender = models.ForeignKey(User, related_name='%(class)s_requests_created', on_delete=models.CASCADE)
    receptor = models.ForeignKey(User, related_name='chat', on_delete=models.CASCADE)
    created_at = models.DateTimeField()