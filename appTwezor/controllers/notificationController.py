from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from appTwezor.models import Notification
from appTwezor.serializers import Notification_Serializer

class NotificationController():
    def crearNotification(notification):
        """Deserializar. convierte my JSON a Objeto(Diccionario o Lista).Para que python pueda trabajar con ellas"""
        notification_datos = JSONParser().parse(notification) 
        """Serializar. convierte my Objeto a JSON"""
        notification_serializada = Notification_Serializer(data=notification_datos)
        if notification_serializada.is_valid():
            notification_serializada.save()
            return JsonResponse(notification_serializada.data, safe=False)
        return JsonResponse("No se pudo crear el team!", safe=False)
    
    def cargarNotificaciones():
        notification = Notification.objects.all()
        """Serializar. convierte my Objeto a JSON"""
        notification_serializada = Notification_Serializer(notification, many = True)
        return JsonResponse(notification_serializada.data, safe=False)

    def modificarNotificacion(notification):
        """Deserializar. convierte my JSON a Objeto(Diccionario o Lista).Para que python pueda trabajar con ellas"""
        notification_datos = JSONParser().parse(notification)
        notification_a_modificar = Notification.objects.get(id = notification_datos["id"])
        """Serializar. convierte my Objeto a JSON"""
        notification_serializada = Notification_Serializer(notification_a_modificar, data=notification_datos)
        if notification_serializada.is_valid():
            notification_serializada.save()
            return JsonResponse(notification_serializada.data, safe=False)
        return JsonResponse("Fallo al tratar de modificar el team.", safe=False)
    
    def eliminarNotificacion(id):
        notification_a_eliminar = Notification.objects.get(id = id)
        notification_a_eliminar.delete()
        return JsonResponse("Team eliminado con exito!", safe=False)
