from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from appTwezor.models import Chat_pg
from appTwezor.serializers import Chat_pgSerializer

class Chat_pgController():
   
    def iniciarChat(chat):
        chat_datos = JSONParser().parse(chat)
        chatSerializado = Chat_pgSerializer(data= chat_datos)
        if chatSerializado.is_valid():
            chatSerializado.save()
            return JsonResponse(chatSerializado.data, safe=False)
        return JsonResponse("No se pudo crear el chat!", safe=False)
    
    def cargarChat():
        chats = Chat_pg.objects.all()
        chats_serializados = Chat_pgSerializer(chats, many = True)
        return JsonResponse(chats_serializados.data, safe=False)

    def modificarChat(chat):
        chat_datos = JSONParser().parse(chat)
        chat_a_modificar = Chat_pg.objects.get(chat_id = chat_datos["chat_id"])
        chat_serializado= Chat_pgSerializer(chat_a_modificar, data=chat_datos)
        if chat_serializado.is_valid():
            chat_serializado.save()
            return JsonResponse(chat_serializado.data, safe=False)
        return JsonResponse("Falló al tratar de modificar el chat.", safe=False)
    
    def terminarChat(id):
        Chat_a_eliminar = Chat_pg.objects.get(chat_id = id)
        Chat_a_eliminar.delete()
        return JsonResponse("Chat finalizado con exito!", safe=False)