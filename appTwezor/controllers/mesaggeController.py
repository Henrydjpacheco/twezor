from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from appTwezor.models import Message
from appTwezor.serializers import Message_Serializer

class MessageController():
    def crearMesagge(message):
        """Deserializar. convierte my JSON a Objeto(Diccionario o Lista).Para que python pueda trabajar con ellas"""
        message_datos = JSONParser().parse(message) 
        """Serializar. convierte my Objeto a JSON"""
        message_serializado = Message_Serializer(data=message_datos)
        if message_serializado.is_valid():
            message_serializado.save()
            return JsonResponse(message_serializado.data, safe=False)
        return JsonResponse("No se pudo crear el team!", safe=False)
    
    def cargarMessage():
        message = Message.objects.all()
        """Serializar. convierte my Objeto a JSON"""
        message_serializado = Message_Serializer(message, many = True)
        return JsonResponse(message_serializado.data, safe=False)

    def modificarMessage(message):
        """Deserializar. convierte my JSON a Objeto(Diccionario o Lista).Para que python pueda trabajar con ellas"""
        message_datos = JSONParser().parse(message)
        message_a_modificar = Message.objects.get(id = message_datos["id"])
        """Serializar. convierte my Objeto a JSON"""
        message_serializado = Message_Serializer(message_a_modificar, data=message_datos)
        if message_serializado.is_valid():
            message_serializado.save()
            return JsonResponse(message_serializado.data, safe=False)
        return JsonResponse("Falló al tratar de modificar el team.", safe=False)
    
    def eliminarMessage(id):
        message_a_eliminar = Message.objects.get(id = id)
        message_a_eliminar.delete()
        return JsonResponse("Team eliminado con exito!", safe=False)
