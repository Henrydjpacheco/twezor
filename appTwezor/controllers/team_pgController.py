from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from appTwezor.models import Team_pg
from appTwezor.serializers import Team_pgSerializer

class Team_pgController():
    def crearTeam(team):
        team_datos = JSONParser().parse(team)
        team_serializado = Team_pgSerializer(data=team_datos)
        if team_serializado.is_valid():
            team_serializado.save()
            return JsonResponse(team_serializado.data, safe=False)
        return JsonResponse("No se pudo crear el team!", safe=False)
    
    def cargarTeams():
        teams = Team_pg.objects.all()
        teams_serializados = Team_pgSerializer(teams, many = True)
        return JsonResponse(teams_serializados.data, safe=False)

    def modificarTeam(team):
        team_datos = JSONParser().parse(team)
        team_a_modificar = Team_pg.objects.get(team_id = team_datos["team_id"])
        team_serializado = Team_pgSerializer(team_a_modificar, data=team_datos)
        if team_serializado.is_valid():
            team_serializado.save()
            return JsonResponse(team_serializado.data, safe=False)
        return JsonResponse("Falló al tratar de modificar el team.", safe=False)
    
    def eliminarTeam(id):
        team_a_eliminar = Team_pg.objects.get(team_id = id)
        team_a_eliminar.delete()
        return JsonResponse("Team eliminado con exito!", safe=False)