from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from appTwezor.models import Comment
from appTwezor.serializers import Comment_Serializer

class CommentController():
    def crearComment(comment):
        """Deserializar. convierte my JSON a Objeto(Diccionario o Lista).Para que python pueda trabajar con ellas"""
        comment_datos = JSONParser().parse(comment) 
        """Serializar. convierte my Objeto a JSON"""
        comment_serializado =   Comment_Serializer(data=comment_datos)
        if comment_serializado.is_valid():
            comment_serializado.save()
            return JsonResponse(comment_serializado.data, safe=False)
        return JsonResponse("No se pudo crear el team!", safe=False)
    
    def cargarComment():
        comment = Comment.objects.all()
        """Serializar. convierte my Objeto a JSON"""
        comment_serializado = Comment_Serializer(comment, many = True)
        return JsonResponse(comment_serializado.data, safe=False)

    def modificarComment(comment):
        """Deserializar. convierte my JSON a Objeto(Diccionario o Lista).Para que python pueda trabajar con ellas"""
        comment_datos = JSONParser().parse(comment)
        comment_a_modificar = Comment.objects.get(id = comment_datos["id"])
        """Serializar. convierte my Objeto a JSON"""
        comment_serializado = Comment_Serializer(comment_a_modificar, data=comment_datos)
        if comment_serializado.is_valid():
            comment_serializado.save()
            return JsonResponse(comment_serializado.data, safe=False)
        return JsonResponse("Falló al tratar de modificar el team.", safe=False)
    
    def eliminarComment(id):
        comment_a_eliminar = Comment.objects.get(id = id)
        comment_a_eliminar.delete()
        return JsonResponse("Team eliminado con exito!", safe=False)
