from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from appTwezor.models import Foro_pg
from appTwezor.serializers import Foro_pgSerializer

class Foro_pgController():
   
    def iniciarForo(foro):
        foro_datos = JSONParser().parse(foro)
        foroSerializado = Foro_pgSerializer(data= foro_datos)
        if foroSerializado.is_valid():
            foroSerializado.save()
            return JsonResponse(foroSerializado.data, safe=False)
        return JsonResponse("No se pudo crear el foro!", safe=False)
    
    def cargarForo():
        foros = Foro_pg.objects.all()
        foros_serializados = Foro_pgSerializer(foros, many = True)
        return JsonResponse(foros_serializados.data, safe=False)

    def modificarForo(team):
        foro_datos = JSONParser().parse(team)
        foro_a_modificar = Foro_pg.objects.get(foro_id = foro_datos["foro_id"])
        foro_serializado = Foro_pgSerializer(foro_a_modificar, data=foro_datos)
        if foro_serializado.is_valid():
            foro_serializado.save()
            return JsonResponse(foro_serializado.data, safe=False)
        return JsonResponse("Falló al tratar de modificar el foro.", safe=False)
    
    def terminarForo(id):
        foro_a_eliminar = Foro_pg.objects.get(foro_id = id)
        foro_a_eliminar.delete()
        return JsonResponse("Foro Terminado con exito!", safe=False)