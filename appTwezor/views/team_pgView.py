from django.views.decorators.csrf import csrf_exempt
from appTwezor.controllers import Team_pgController

class Team_pgView():
    @csrf_exempt
    def team_pgAPI(request, id = 0):
        if request.method == 'POST':
            return Team_pgController.crearTeam(request)
        elif request.method == 'GET':
            return Team_pgController.cargarTeams()
        elif request.method == 'PUT':
            return Team_pgController.modificarTeam(request)
        elif request.method == 'DELETE':
            return Team_pgController.eliminarTeam(id)