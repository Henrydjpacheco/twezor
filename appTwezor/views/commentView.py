from django.views.decorators.csrf import csrf_exempt
from appTwezor.controllers import CommentController

class CommentView():
    @csrf_exempt
    def commentAPI(request, id = 0):
        if request.method == 'POST':
            return CommentController.crearComment(request)
        elif request.method == 'GET':
            return CommentController.cargarComment()
        elif request.method == 'PUT':
            return CommentController.modificarComment(request)
        elif request.method == 'DELETE':
            return CommentController.eliminarComment(id)
