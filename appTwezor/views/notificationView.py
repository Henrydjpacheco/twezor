from django.views.decorators.csrf import csrf_exempt
from appTwezor.controllers import NotificationController

class NotificationView():
    @csrf_exempt
    def notificationAPI(request, id = 0):
        if request.method == 'POST':
            return NotificationController.crearNotification(request)
        elif request.method == 'GET':
            return NotificationController.cargarNotificaciones()
        elif request.method == 'PUT':
            return NotificationController.modificarNotificacion(request)
        elif request.method == 'DELETE':
            return NotificationController.eliminarNotificacion(id)
