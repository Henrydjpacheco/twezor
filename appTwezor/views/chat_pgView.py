from django.views.decorators.csrf import csrf_exempt
from appTwezor.controllers import Chat_pgController

class Chat_pgView():
    @csrf_exempt
    def chat_pgAPI(request, id = 0):
        if request.method == 'POST':
            return Chat_pgController.iniciarChat(request)
        elif request.method == 'GET':
            return Chat_pgController.cargarChat()
        elif request.method == 'PUT':
            return Chat_pgController.modificarChat(request)
        elif request.method == 'DELETE':
            return Chat_pgController.terminarChat(id)