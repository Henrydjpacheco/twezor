from django.views.decorators.csrf import csrf_exempt
from appTwezor.controllers import MessageController

class MessageView():
    @csrf_exempt
    def messageAPI(request, id = 0):
        if request.method == 'POST':
            return MessageController.crearMesagge(request)
        elif request.method == 'GET':
            return MessageController.cargarMessage()
        elif request.method == 'PUT':
            return MessageController.modificarMessage(request)
        elif request.method == 'DELETE':
            return MessageController.eliminarMessage(id)
