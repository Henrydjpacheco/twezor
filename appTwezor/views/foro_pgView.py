from django.views.decorators.csrf import csrf_exempt
from appTwezor.controllers import Foro_pgController

class Foro_pgView():
    @csrf_exempt
    def foro_pgAPI(request, id = 0):
        if request.method == 'POST':
            return Foro_pgController.crearForo(request)
        elif request.method == 'GET':
            return Foro_pgController.cargarForo()
        elif request.method == 'PUT':
            return Foro_pgController.modificarForo(request)
        elif request.method == 'DELETE':
            return Foro_pgController.terminarForo(id)