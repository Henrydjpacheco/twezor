from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .chat_pgView import Chat_pgView
from .team_pgView import Team_pgView
from .foro_pgView import Foro_pgView
from .commentView import CommentView
from .messageView import MessageView
from .notificationView import NotificationView