"""authTwezor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path


from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from appTwezor import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('team', views.Team_pgView.team_pgAPI),
    path('team/<int:id>', views.Team_pgView.team_pgAPI),
    path('foro', views.Foro_pgView.foro_pgAPI),
    path('foro/<int:id>', views.Foro_pgView.foro_pgAPI),
    path('chat', views.Chat_pgView.chat_pgAPI),
    path('chat/<int:id>', views.Chat_pgView.chat_pgAPI),
    path('notification', views.NotificationView.notificationAPI),
    path('notification/<int:id>', views.NotificationView.notificationAPI),
    path('comment', views.CommentView.commentAPI),
    path('comment/<int:id>', views.CommentView.commentAPI),
    path('message', views.MessageView.messageAPI),
    path('message/<int:id>', views.MessageView.messageAPI)

]
    